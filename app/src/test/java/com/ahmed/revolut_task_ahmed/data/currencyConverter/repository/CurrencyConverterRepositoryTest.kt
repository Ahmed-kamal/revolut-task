package com.ahmed.revolut_task_ahmed.data.currencyConverter.repository

import com.ahmed.revolut_task_ahmed.data.common.DataResult
import com.ahmed.revolut_task_ahmed.data.currencyConverter.model.CurrencyConverterResponse
import com.ahmed.revolut_task_ahmed.data.currencyConverter.model.CurrencyRvModel
import com.ahmed.revolut_task_ahmed.data.currencyConverter.model.Rates
import com.ahmed.revolut_task_ahmed.data.currencyConverter.remoteDataSource.CurrencyConverterRemoteDataSource
import com.ahmed.revolut_task_ahmed.data.currencyConverter.remoteDataSource.CurrencyConverterService
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import java.math.BigDecimal

@RunWith(MockitoJUnitRunner::class)
class CurrencyConverterRepositoryTest {

    @Mock
    lateinit var service: CurrencyConverterService

    @Mock
    lateinit var remoteDataSource: CurrencyConverterRemoteDataSource

    private lateinit var repository: CurrencyConverterRepository

    private var response = getCurrencyResponse()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        service = mock(CurrencyConverterService::class.java)

        remoteDataSource = mock(CurrencyConverterRemoteDataSource::class.java)

        Mockito.`when`(service.getCurrency(ArgumentMatchers.anyString())).thenReturn(Single.just(response))

        Mockito.`when`(remoteDataSource.getCurrency(ArgumentMatchers.anyString())).thenReturn(
            Single.just(response))

        repository = CurrencyConverterRepositoryImpl(remoteDataSource)
    }


    @Test
    fun `when getting currencies , should success`() {
        val currency = "EUR"
        val currencySuccessObserver = TestObserver<DataResult<List<CurrencyRvModel>>>()

        val currencyResult = repository.getCurrency(currency, BigDecimal.ONE)
        currencyResult.subscribe(currencySuccessObserver)

        assertNotNull(currencyResult)
        val result = currencySuccessObserver.values()[0] as DataResult.Success
        assertEquals(result.result[0].currency, "EUR")
        assertEquals(result.result[0].amount, BigDecimal.ONE)
    }

    @Test
    fun `when converting, should success`() {
        val currency = "EUR"
        val value = BigDecimal.TEN
        val currencySuccessObserver = TestObserver<DataResult<List<CurrencyRvModel>>>()

        val currencyResult = repository.getCurrency(currency, value)
        currencyResult.subscribe(currencySuccessObserver)

        assertNotNull(currencyResult)
        val result = currencySuccessObserver.values()[0] as DataResult.Success
        assertEquals(result.result[0].currency, "EUR")
        assertEquals(result.result[0].amount, value)

        assertEquals(result.result[1].currency, "SEK")
        assertEquals(result.result[1].amount, BigDecimal("1.87") * value)

        assertEquals(result.result[2].currency, "ZAR")
        assertEquals(result.result[2].amount, BigDecimal("3.54") * value)

        assertEquals(result.result[3].currency, "RON")
        assertEquals(result.result[3].amount, BigDecimal("0.894") * value)

        assertEquals(result.result[4].currency, "BGN")
        assertEquals(result.result[4].amount, BigDecimal("1.9987") * value)

        assertEquals(result.result[5].currency, "JPY")
        assertEquals(result.result[5].amount, BigDecimal("12.87") * value)

        assertEquals(result.result[6].currency, "PHP")
        assertEquals(result.result[6].amount, BigDecimal("4.564") * value)
    }

    private fun getCurrencyResponse(): CurrencyConverterResponse {
        val currencyMap = HashMap<Rates?, Float>()
        currencyMap[Rates.JPY] = 12.87f
        currencyMap[Rates.SEK] = 1.87f
        currencyMap[Rates.BGN] = 1.9987f
        currencyMap[Rates.RON] = 0.894f
        currencyMap[Rates.PHP] = 4.564f
        currencyMap[Rates.ZAR] = 3.54f

        return CurrencyConverterResponse("EUR", currencyMap)
    }
}
package com.ahmed.revolut_task_ahmed.data.currencyConverter.repository

import com.ahmed.revolut_task_ahmed.data.common.DataResult
import com.ahmed.revolut_task_ahmed.data.common.DataSource
import com.ahmed.revolut_task_ahmed.data.currencyConverter.model.CurrencyRvModel
import com.ahmed.revolut_task_ahmed.data.currencyConverter.remoteDataSource.CurrencyConverterRemoteDataSource
import com.ahmed.revolut_task_ahmed.platform.extenstion.dropSearch
import io.reactivex.Single
import java.math.BigDecimal

class CurrencyConverterRepositoryImpl (private val remoteDataSource: CurrencyConverterRemoteDataSource):
    CurrencyConverterRepository {

    override fun getCurrency(currencyType: String, baseNumber: BigDecimal): Single<DataResult<List<CurrencyRvModel>>> =
        remoteDataSource.getCurrency(currencyType).dropSearch().map {
            val rvList = ArrayList<CurrencyRvModel>()
            rvList.add(CurrencyRvModel(currencyType, baseNumber))
            it.rates.map { currencies ->
                currencies.key?.let {
                    rvList.add(CurrencyRvModel(currencies.key.toString(), (currencies.value.toBigDecimal() * baseNumber)))
                }
            }

            return@map DataResult.Success(DataSource.Network, rvList) as DataResult<List<CurrencyRvModel>>
        }.onErrorReturn { DataResult.Error(DataSource.Network, it) }

}
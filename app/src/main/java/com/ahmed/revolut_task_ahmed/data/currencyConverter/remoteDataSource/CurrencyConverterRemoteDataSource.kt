package com.ahmed.revolut_task_ahmed.data.currencyConverter.remoteDataSource

import com.ahmed.revolut_task_ahmed.data.rx.RxSingleCallback
import com.ahmed.revolut_task_ahmed.data.currencyConverter.model.CurrencyConverterResponse
import io.reactivex.Single

class CurrencyConverterRemoteDataSource (private val service: CurrencyConverterService) {
    fun getCurrency(currency: String): Single<CurrencyConverterResponse> =
        service.getCurrency(currency).compose(RxSingleCallback.getSchedulersForSingle())
}
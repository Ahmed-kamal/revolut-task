package com.ahmed.revolut_task_ahmed.data.currencyConverter.model

import java.math.BigDecimal

data class CurrencyRvModel(val currency: String, val amount: BigDecimal)
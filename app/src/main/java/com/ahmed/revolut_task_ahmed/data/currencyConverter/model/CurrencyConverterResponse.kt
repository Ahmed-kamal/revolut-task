package com.ahmed.revolut_task_ahmed.data.currencyConverter.model

data class CurrencyConverterResponse(
    val baseCurrency: String,
    val rates: Map<Rates?, Float>
)
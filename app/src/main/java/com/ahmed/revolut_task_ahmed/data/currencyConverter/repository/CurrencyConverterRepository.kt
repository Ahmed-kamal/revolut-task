package com.ahmed.revolut_task_ahmed.data.currencyConverter.repository

import com.ahmed.revolut_task_ahmed.data.common.DataResult
import com.ahmed.revolut_task_ahmed.data.currencyConverter.model.CurrencyConverterResponse
import com.ahmed.revolut_task_ahmed.data.currencyConverter.model.CurrencyRvModel
import io.reactivex.Single
import java.math.BigDecimal

interface CurrencyConverterRepository {
    fun getCurrency(currencyType: String, baseNumber: BigDecimal): Single<DataResult<List<CurrencyRvModel>>>
}
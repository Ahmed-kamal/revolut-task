package com.ahmed.revolut_task_ahmed.data.currencyConverter.remoteDataSource

import com.ahmed.revolut_task_ahmed.data.currencyConverter.model.CurrencyConverterResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyConverterService {
    @GET("api/android/latest")
    fun getCurrency(@Query("base") currencyType: String): Single<CurrencyConverterResponse>
}
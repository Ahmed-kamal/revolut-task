package com.ahmed.revolut_task_ahmed.util

import com.ahmed.revolut_task_ahmed.R
import com.ahmed.revolut_task_ahmed.data.currencyConverter.model.Rates

object CurrencyResources {

    fun getCurrencyImage(currency: Rates) = when (currency) {
        Rates.AUD -> R.drawable.ic_australia
        Rates.BGN -> R.drawable.ic_bulgaria
        Rates.BRL -> R.drawable.ic_brazil
        Rates.CAD -> R.drawable.ic_canada
        Rates.CHF -> R.drawable.ic_switzerland
        Rates.CNY -> R.drawable.ic_china
        Rates.CZK -> R.drawable.ic_czech_republic
        Rates.DKK -> R.drawable.ic_denmark
        Rates.EUR -> R.drawable.ic_european_union
        Rates.GBP -> R.drawable.ic_uk
        Rates.HKD -> R.drawable.ic_hong_kong
        Rates.HRK -> R.drawable.ic_croatia
        Rates.HUF -> R.drawable.ic_hungary
        Rates.IDR -> R.drawable.ic_indonesia
        Rates.ILS -> R.drawable.ic_israel
        Rates.INR -> R.drawable.ic_india
        Rates.ISK -> R.drawable.ic_iceland
        Rates.JPY -> R.drawable.ic_japan
        Rates.KRW -> R.drawable.ic_south_korea
        Rates.MXN -> R.drawable.ic_mexico
        Rates.MYR -> R.drawable.ic_malaysia
        Rates.NOK -> R.drawable.ic_norway
        Rates.NZD -> R.drawable.ic_new_zealand
        Rates. PHP -> R.drawable.ic_philippines
        Rates.PLN -> R.drawable.ic_poland
        Rates.RON -> R.drawable.ic_romania
        Rates.RUB -> R.drawable.ic_russia
        Rates.SEK -> R.drawable.ic_sweden
        Rates.SGD -> R.drawable.ic_singapore
        Rates.THB -> R.drawable.ic_thailand
        Rates.USD -> R.drawable.ic_usa
        Rates.ZAR -> R.drawable.ic_south_africa
    }

    fun getCurrencyFullName(currency: Rates) = when (currency) {
        Rates.AUD -> R.string.aud_currency
        Rates.BGN -> R.string.bgn_currency
        Rates.BRL -> R.string.brl_currency
        Rates.CAD -> R.string.cad_currency
        Rates.CHF -> R.string.chf_currency
        Rates.CNY -> R.string.cny_currency
        Rates.CZK -> R.string.czk_currency
        Rates.DKK -> R.string.dkk_currency
        Rates.EUR -> R.string.eur_currency
        Rates.GBP -> R.string.gbp_currency
        Rates.HKD -> R.string.hkd_currency
        Rates.HRK -> R.string.hrk_currency
        Rates.HUF -> R.string.huf_currency
        Rates.IDR -> R.string.idr_currency
        Rates.ILS -> R.string.ils_currency
        Rates.INR -> R.string.inr_currency
        Rates.ISK -> R.string.isk_currency
        Rates.JPY -> R.string.jpy_currency
        Rates.KRW -> R.string.krw_currency
        Rates. MXN -> R.string.mxn_currency
        Rates.MYR -> R.string.myr_currency
        Rates.NOK -> R.string.nok_currency
        Rates.NZD -> R.string.nzd_currency
        Rates.PHP -> R.string.php_currency
        Rates.PLN -> R.string.pln_currency
        Rates.RON -> R.string.ron_currency
        Rates.RUB -> R.string.rub_currency
        Rates.SEK -> R.string.sek_currency
        Rates.SGD -> R.string.sgd_currency
        Rates.THB -> R.string.thb_currency
        Rates.USD -> R.string.usd_currency
        Rates.ZAR -> R.string.zar_currency
    }
}
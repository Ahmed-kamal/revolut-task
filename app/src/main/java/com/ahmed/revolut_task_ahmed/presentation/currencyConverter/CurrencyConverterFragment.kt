package com.ahmed.revolut_task_ahmed.presentation.currencyConverter

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.ahmed.revolut_task_ahmed.R
import com.ahmed.revolut_task_ahmed.databinding.FragmentCurrencyConverterBinding
import com.ahmed.revolut_task_ahmed.platform.bases.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.math.BigDecimal


class CurrencyConverterFragment : BaseFragment<FragmentCurrencyConverterBinding>() {

    private val viewModel: CurrencyConverterViewModel by viewModel()

    override fun layoutId(): Int = R.layout.fragment_currency_converter

    private val adapter = CurrencyConverterRvAdapter({
        viewModel.getCurrency(it, BigDecimal.ONE)
        viewModel.isScrollable = true

    },{ currency, value ->
        viewModel.getCurrency(currency.currency, value)
    })


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.get()?.let { bindings ->
            bindings.viewModel = viewModel
            bindings.rvCurrencyList.adapter = adapter
            bindings.rvCurrencyList.itemAnimator?.endAnimations()
            bindings.currencyRetry.setOnClickListener { viewModel.getCurrency("EUR", BigDecimal.ONE) }
        }

        viewModel.scrollToTopLiveEvent.observe(viewLifecycleOwner, Observer {
            if (it) binding.get()?.rvCurrencyList?.smoothScrollToPosition(0)
            viewModel.isScrollable = false
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.get()?.rvCurrencyList?.adapter = null
    }
}
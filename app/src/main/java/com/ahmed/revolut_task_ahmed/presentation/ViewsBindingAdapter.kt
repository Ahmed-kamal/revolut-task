package com.ahmed.revolut_task_ahmed.presentation

import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.ahmed.revolut_task_ahmed.data.currencyConverter.model.CurrencyRvModel
import com.ahmed.revolut_task_ahmed.platform.bases.BaseViewState
import com.ahmed.revolut_task_ahmed.presentation.currencyConverter.CurrencyConverterRvAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

@BindingAdapter("loadImageFromUrl")
fun loadImageFromUrl(view: ImageView, url: String?) {
    Glide.with(view.context).load(url).
    diskCacheStrategy(DiskCacheStrategy.ALL).
    transition(DrawableTransitionOptions.withCrossFade()).
    into(view)
}

@BindingAdapter("bindingViewsErrorState")
fun <T> bindingViewsErrorState(view: TextView, viewState: BaseViewState<List<T>>) {
    if(viewState is BaseViewState.Error) { view.visibility = View.VISIBLE; view.text = viewState.message } else view.visibility = View.GONE
}

@BindingAdapter("bindingViewsLoadingState")
fun <T> bindingViewsLoadingState(view: ProgressBar, viewState: BaseViewState<List<T>>) {
    view.visibility =  if(viewState is BaseViewState.Loading) View.VISIBLE else View.GONE
}


@BindingAdapter("bindingViewsRetryState")
fun <T> bindingViewsRetryState(view: Button, viewState: BaseViewState<T>) {
    view.visibility =  if(viewState is BaseViewState.Retry) View.VISIBLE else View.GONE
}

@BindingAdapter("bindingViewsObjectErrorState")
fun <T> bindingViewsObjectErrorState(view: TextView, viewState: BaseViewState<T>) {
    if(viewState is BaseViewState.Error) { view.visibility = View.VISIBLE; view.text = viewState.message } else view.visibility = View.GONE
}

@BindingAdapter("bindingViewsObjectLoadingState")
fun <T> bindingViewsObjectLoadingState(view: ProgressBar, viewState: BaseViewState<T>) {
    view.visibility =  if(viewState is BaseViewState.Loading) View.VISIBLE else View.GONE
}

@BindingAdapter("bindingAdapterSuccessState")
fun <T> bindingAdapterSuccessState(rv: RecyclerView, viewState: BaseViewState<List<T>>) {
    val adapter = rv.adapter
    if(viewState is BaseViewState.Success) {
        rv.visibility = View.VISIBLE
        when (adapter) {
            is CurrencyConverterRvAdapter -> {
                adapter.submitList(viewState.data as List<CurrencyRvModel>)
            }
        }
    } else if(viewState is BaseViewState.MoveToTop) {
        rv.smoothScrollToPosition(0)
    } else {
        rv.visibility = View.GONE
    }
}
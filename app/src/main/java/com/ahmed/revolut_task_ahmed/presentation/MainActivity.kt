package com.ahmed.revolut_task_ahmed.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ahmed.revolut_task_ahmed.R
import com.ahmed.revolut_task_ahmed.presentation.currencyConverter.CurrencyConverterFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
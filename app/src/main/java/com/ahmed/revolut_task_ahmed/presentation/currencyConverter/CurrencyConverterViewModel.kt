package com.ahmed.revolut_task_ahmed.presentation.currencyConverter

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.ahmed.revolut_task_ahmed.data.common.DataResult
import com.ahmed.revolut_task_ahmed.data.currencyConverter.model.CurrencyRvModel
import com.ahmed.revolut_task_ahmed.domain.currencyConverter.CurrencyConverterUseCase
import com.ahmed.revolut_task_ahmed.platform.bases.BaseViewModel
import com.ahmed.revolut_task_ahmed.platform.bases.BaseViewState
import com.ahmed.revolut_task_ahmed.platform.livedata.SingleLiveEvent
import java.math.BigDecimal

class CurrencyConverterViewModel(application: Application, private val useCase: CurrencyConverterUseCase): BaseViewModel(application) {

    val currencyViewState: MutableLiveData<BaseViewState<List<CurrencyRvModel>>> = MutableLiveData()
    val scrollToTopLiveEvent: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var isScrollable = false

    init {
        currencyViewState.value = BaseViewState.Loading()
        getCurrency("EUR", BigDecimal.ONE)
    }

    fun getCurrency(currencyType: String, baseConverter: BigDecimal) {
        compositeDisposable.clear()
        compositeDisposable.add(useCase.getCurrency(currencyType, baseConverter).subscribe{ dataResult ->
            when(dataResult) {
                is DataResult.Success -> {
                    currencyViewState.value = BaseViewState.Success(dataResult.result)
                    scrollToTopLiveEvent.value = isScrollable
                }
                is DataResult.Error -> {
                    compositeDisposable.clear()
                    currencyViewState.value = BaseViewState.Error(dataResult.throwable.message ?: "Error")
                    currencyViewState.value = BaseViewState.Retry()
                }
            }
        })
    }
}
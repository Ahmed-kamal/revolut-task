package com.ahmed.revolut_task_ahmed.presentation.currencyConverter

import com.ahmed.revolut_task_ahmed.data.currencyConverter.remoteDataSource.CurrencyConverterRemoteDataSource
import com.ahmed.revolut_task_ahmed.data.currencyConverter.remoteDataSource.CurrencyConverterService
import com.ahmed.revolut_task_ahmed.data.currencyConverter.repository.CurrencyConverterRepository
import com.ahmed.revolut_task_ahmed.data.currencyConverter.repository.CurrencyConverterRepositoryImpl
import com.ahmed.revolut_task_ahmed.domain.currencyConverter.CurrencyConverterUseCase
import com.ahmed.revolut_task_ahmed.domain.currencyConverter.CurrencyConverterUseCaseImpl
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val currencyConverterModule = module {
    single<CurrencyConverterRepository> { CurrencyConverterRepositoryImpl(get()) }
    single<CurrencyConverterUseCase> {  CurrencyConverterUseCaseImpl(get()) }
    single { CurrencyConverterRemoteDataSource(get()) }
    single { get<Retrofit>().create(CurrencyConverterService::class.java) }
    viewModel { CurrencyConverterViewModel(get(), get()) }
}

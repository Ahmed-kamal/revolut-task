package com.ahmed.revolut_task_ahmed.presentation.currencyConverter

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ahmed.revolut_task_ahmed.R
import com.ahmed.revolut_task_ahmed.data.currencyConverter.model.CurrencyRvModel
import com.ahmed.revolut_task_ahmed.data.currencyConverter.model.Rates
import com.ahmed.revolut_task_ahmed.databinding.ItemRateBinding
import com.ahmed.revolut_task_ahmed.util.CurrencyResources
import java.math.BigDecimal
import java.util.*

typealias onCurrencyClicked = (currency: String) -> Unit
typealias onCurrencyEdited = (CurrencyRvModel, BigDecimal) -> Unit

class CurrencyConverterRvAdapter(private val currencyClicked: onCurrencyClicked, private val onCurrencyEdited: onCurrencyEdited): ListAdapter<CurrencyRvModel,

        CurrencyConverterRvAdapter.CurrencyViewHolder>(CurrencyConverterDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder = CurrencyViewHolder(
        DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_rate, parent, false))

    override fun onBindViewHolder(viewHolder: CurrencyViewHolder, position: Int) {
        getItem(position)?.let {
            with(viewHolder) {
                itemView.tag = it.amount
                bind(it, currencyClicked, position)
            }
        }
    }

    inner class CurrencyViewHolder(private val binding: ItemRateBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(
            rates: CurrencyRvModel,
            currencyClicked: onCurrencyClicked,
            position: Int) {

            with(binding) {
                rvViewModel = CurrencyConverterRvViewModel(rates)
                itemAmount.isEnabled = position == 0
                if(itemAmount.isEnabled) itemAmount.requestFocus()
                itemCurrencyIv.setImageResource(CurrencyResources.getCurrencyImage(Rates.valueOf(rates.currency)))
                itemCurrencyDesc.setText(CurrencyResources.getCurrencyFullName(Rates.valueOf(rates.currency)))

                itemView.setOnClickListener {
                    moveItemToFirst(position)
                    currencyClicked.invoke(rates.currency)
                }

                binding.itemAmount.addTextChangedListener(object: TextWatcher {
                    override fun afterTextChanged(p0: Editable?) {
                        if(!p0.isNullOrBlank() && binding.itemAmount.isEnabled) {
                            onCurrencyEdited.invoke(rates, BigDecimal(p0.toString()))
                        }
                    }

                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                })
                executePendingBindings()
            }
        }
    }

    fun moveItemToFirst(fromPosition: Int) {
        if(fromPosition != 0) {
            currentList.toMutableList().removeAt(fromPosition).also { rate ->
                currentList.toMutableList().add(0, rate)
            }
            notifyItemMoved(fromPosition, 0)
        }
    }
}

class CurrencyConverterRvViewModel(rate: CurrencyRvModel): ViewModel() {
    val currency = ObservableField(rate.currency)
    val amount = ObservableField(rate.amount.toString())
}

class CurrencyConverterDiffCallback: DiffUtil.ItemCallback<CurrencyRvModel>() {
    override fun areItemsTheSame(oldItem: CurrencyRvModel, newItem: CurrencyRvModel): Boolean = oldItem.currency == newItem.currency
    override fun areContentsTheSame(oldItem: CurrencyRvModel, newItem: CurrencyRvModel): Boolean = oldItem.amount == newItem.amount

}
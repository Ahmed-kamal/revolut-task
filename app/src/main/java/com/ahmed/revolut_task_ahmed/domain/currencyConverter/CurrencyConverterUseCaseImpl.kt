package com.ahmed.revolut_task_ahmed.domain.currencyConverter

import com.ahmed.revolut_task_ahmed.data.common.DataResult
import com.ahmed.revolut_task_ahmed.data.currencyConverter.model.CurrencyConverterResponse
import com.ahmed.revolut_task_ahmed.data.currencyConverter.model.CurrencyRvModel
import com.ahmed.revolut_task_ahmed.data.currencyConverter.repository.CurrencyConverterRepository
import io.reactivex.Flowable
import io.reactivex.Single
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

class CurrencyConverterUseCaseImpl(private val repository: CurrencyConverterRepository):
        CurrencyConverterUseCase {

    override fun getCurrency(currencyType: String, baseConverter: BigDecimal): Flowable<DataResult<List<CurrencyRvModel>>> =
        Flowable.interval(1, TimeUnit.SECONDS).flatMap { return@flatMap repository.getCurrency(currencyType, baseConverter).toFlowable() }

}
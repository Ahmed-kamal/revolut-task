package com.ahmed.revolut_task_ahmed.domain.currencyConverter

import com.ahmed.revolut_task_ahmed.data.common.DataResult
import com.ahmed.revolut_task_ahmed.data.currencyConverter.model.CurrencyConverterResponse
import com.ahmed.revolut_task_ahmed.data.currencyConverter.model.CurrencyRvModel
import io.reactivex.Flowable
import io.reactivex.Single
import java.math.BigDecimal

interface CurrencyConverterUseCase {
    fun getCurrency(currencyType: String, baseConverter: BigDecimal): Flowable<DataResult<List<CurrencyRvModel>>>
}
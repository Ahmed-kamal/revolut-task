package com.ahmed.revolut_task_ahmed.platform.bases

sealed class BaseViewState<E> {
    class Success<E>(val data: E): BaseViewState<E>()
    class Loading<E>: BaseViewState<E>()
    class Error<E>(val message: String): BaseViewState<E>()
    class Retry<E>: BaseViewState<E>()
    class MoveToTop<E>: BaseViewState<E>()
}
package com.ahmed.revolut_task_ahmed.platform.extenstion

import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.exceptions.CompositeException

fun <T> Flowable<T>.dropSearch(): Flowable<T> {
    return this.onErrorResumeNext { error: Throwable ->
        throw CompositeException(error)
    }
}

fun <T> Single<T>.dropSearch(): Single<T> {
    return this.onErrorResumeNext { error: Throwable ->
        throw CompositeException(error)
    }
}

package com.ahmed.revolut_task_ahmed.platform.bases

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import java.lang.ref.WeakReference

abstract class BaseActivity<B: ViewDataBinding>: AppCompatActivity() {

    @LayoutRes
    abstract fun layoutId(): Int
    lateinit var binding: WeakReference<B>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = WeakReference(DataBindingUtil.setContentView(this, layoutId()))
    }
}
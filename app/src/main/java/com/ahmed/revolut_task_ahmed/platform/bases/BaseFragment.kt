package com.ahmed.revolut_task_ahmed.platform.bases

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import java.lang.ref.WeakReference

abstract class BaseFragment<B: ViewDataBinding>: Fragment() {

    @LayoutRes
    abstract fun layoutId(): Int
    lateinit var binding: WeakReference<B>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = WeakReference(DataBindingUtil.inflate(layoutInflater, layoutId(), container, false))
        return binding.get()?.also { it.lifecycleOwner = this }?.root
    }

    fun showToast(msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
    }

    fun onBackClicked() {
        findNavController().navigateUp()
    }
}
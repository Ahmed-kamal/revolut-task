package com.ahmed.revolut_task_ahmed.app.injection

import com.ahmed.revolut_task_ahmed.presentation.currencyConverter.currencyConverterModule
import org.koin.core.module.Module

val appComponent: List<Module> = listOf(networkModule, currencyConverterModule)
package com.ahmed.revolut_task_ahmed.app

import android.app.Application
import com.ahmed.revolut_task_ahmed.BuildConfig
import com.ahmed.revolut_task_ahmed.app.injection.appComponent
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class AndroidApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        setup()
    }

    private fun setup() {
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
        initKoin()
    }

    private fun initKoin() {
        startKoin{
            androidLogger()
            androidContext(this@AndroidApplication)
            modules(appComponent)
        }
    }

}
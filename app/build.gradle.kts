plugins {
    id(AndroidPlugins.androidApplication)
    kotlin(AndroidPlugins.android)
    kotlin(AndroidPlugins.androidExtension)
    kotlin(AndroidPlugins.kapt)
    id(AndroidPlugins.navigation)
    id(AndroidPlugins.kotlinAndroid)
}

android {
    compileSdkVersion(App.compileVersion)
    buildToolsVersion(App.buildToolsVersion)

    defaultConfig {
        minSdkVersion(App.minSdk)
        targetSdkVersion(App.targetSdk)
        versionCode = App.versionCode
        versionName = App.versionName
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        javaCompileOptions {
            annotationProcessorOptions {
                arguments = mapOf("room.schemaLocation" to "$projectDir/schemas")
            }
        }
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
            buildConfigField("String", Config.BASE_URL, "\"https://hiring.revolut.codes/\"")
        }
        getByName("debug") {
            // This way we could set a different url depending on the variant or flavor
            buildConfigField("String", Config.BASE_URL, "\"https://hiring.revolut.codes/\"")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    buildFeatures { dataBinding = true }

    kapt {
        generateStubs = true
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(Libs.kotlin)

    implementation(Libs.appCompat)
    implementation(Libs.ktx)
    implementation(Libs.lifecycle)
    implementation(Libs.constraintLayout)
    implementation(Libs.material)
    implementation(Libs.navigation)
    implementation(Libs.navigationUi)
    implementation(Libs.koinViewModel)
    implementation(Libs.koinFragment)

    implementation(Libs.lifecycleExtentions)
    implementation(Libs.rxJava)
    implementation(Libs.rxKotlin)
    implementation(Libs.rxAndroid)
    implementation(Libs.room)
    kapt(Libs.roomCompiler)
    implementation(Libs.roomRxJava)
    implementation(Libs.gson)
    implementation(Libs.retrofit)
    implementation(Libs.retrofitGson)
    implementation(Libs.retrofitRx)
    implementation(Libs.glide)
    kapt(Libs.glideCompiler)
    implementation(Libs.lottie)
    implementation(Libs.timber)

    testImplementation(Libs.junit)
    testImplementation(Libs.mockito)
    testImplementation(Libs.mockitoInline)
    testImplementation(Libs.mockitoRetrofit)

    debugImplementation(Libs.fragmentTest)

    androidTestImplementation(Libs.junitAndroid)
    androidTestImplementation(Libs.navigationTesting)
    androidTestImplementation(Libs.coreTesting)
}
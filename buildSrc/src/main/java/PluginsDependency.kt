object AndroidPlugins {
    val androidApplication = "com.android.application"
    val android = "android"
    val androidExtension = "android.extensions"
    val kapt = "kapt"
    val navigation = "androidx.navigation.safeargs.kotlin"
    val kotlinAndroid = "kotlin-android"
}